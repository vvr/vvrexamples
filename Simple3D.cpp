#include "Simple3D.h"
#include <VVRScene/utils.h>
#include <VVRScene/canvas.h>
#include <VVRScene/logger.h>
#include <iostream>
#include <fstream>
#include <iostream>
#include <cstring>
#include <string>
#include <MathGeoLib.h>

#define FLAG_SHOW_AXES 1
#define FLAG_SHOW_AABB 2
#define FLAG_SHOW_WIRE 4
#define FLAG_SHOW_SOLID 8
#define FLAG_SHOW_NORMALS 16

using namespace vvr;
using namespace std;
using namespace math;

#define objName "cube.obj"
#define objName "bunny_low.obj"
#define objName "unicorn_low.obj"
#define objName "dragon_low_low.obj"
#define objName "large/unicorn.obj"
#define objName "ironman.obj"

const float L1 = 25;
const float L2 = 5;
const float h1 = -7;
const float h2 = 13;

const vec A(-L1, h1, -L2);
const vec B(+L1, h1, -L2);
const vec C(+L1, h1, +L2);
const vec D(-L1, h1, +L2);
const vec E(-L1, h2, -L2);
const vec F(+L1, h2, -L2);

Simple3DScene::Simple3DScene()
{
    m_bg_col = Colour("768E77");
    m_obj_col = Colour("454545");
    m_perspective_proj = true;
    
    m_style_flag = 0;
    m_style_flag |= FLAG_SHOW_SOLID;
    m_style_flag |= FLAG_SHOW_WIRE;

    m_floor_tris.push_back(math::Triangle(B, A, D));
    m_floor_tris.push_back(math::Triangle(B, D, C));
    m_floor_tris.push_back(math::Triangle(F, E, A));
    m_floor_tris.push_back(math::Triangle(F, A, B));
}

void Simple3DScene::reset()
{
    Scene::reset();
    auto pos = getFrustum().Pos();
    pos.y += 10;
    pos.z -= 40;
    setCameraPos(pos);
    m_canvas.clear();
}

void Simple3DScene::resize()
{
    static bool first_pass = true;

    if (first_pass)
    {
        reset();
        load3DModels();
    }

    first_pass = false;
}

void Simple3DScene::load3DModels()
{
    // Load 3D models.
    const string objFile = getBasePath() + "resources/obj/" + objName;

    m_mesh_1 = new Mesh(objFile);
    m_mesh_1->cornerAlign();
    m_mesh_1->setBigSize(getSceneWidth() / 8);
    m_mesh_1->update();

    m_mesh_2 = new Mesh(*m_mesh_1);
    m_mesh_3 = new Mesh(*m_mesh_1);
    
    m_mesh_2->setBigSize(m_mesh_1->getMaxSize() * 1.5);
    m_mesh_3->setBigSize(m_mesh_1->getMaxSize() / 1.5);

    m_mesh_1->move(vec(m_mesh_1->getAABB().Size().x * -0.5, h1, 0));
    m_mesh_2->move(vec(m_mesh_1->getAABB().Size().x * +0.5, h1, 0));
    m_mesh_3->move(vec(m_mesh_1->getAABB().Size().x * -0.5 + m_mesh_3->getAABB().Size().x * -1, h1, 0));

    m_mesh_1->update();
    m_mesh_2->update();
    m_mesh_3->update();
}

void Simple3DScene::playWithMathGeoLibPolygon()
{

    math::Polyhedron poly;
    vvr::Mesh* mesh = m_mesh_1;

    for (unsigned i = 0; i < mesh->getVertices().size(); ++i) {
        vec &v = mesh->getVertices()[i];
        poly.v.push_back({ float3(v.x, v.y, v.z) });
    }

    for (unsigned i = 0; i < mesh->getTriangles().size(); ++i) {
        vvr::Triangle &t = mesh->getTriangles()[i];
        Polyhedron::Face f;
        f.v = { t.vi1, t.vi2, t.vi3 };
        f.FlipWindingOrder();
        poly.f.push_back(f);
    }

    echo(poly.IsConvex());
}

void Simple3DScene::draw()
{
    //! Draw floor
    for (const auto &tri : m_floor_tris)
    {
        auto floor_tri = math2vvr(tri, Colour(23, 35, 56));
        floor_tri.setSolidRender(true);
        floor_tri.draw();
    }

    //! Draw meshes
    for (auto mesh : { m_mesh_1, m_mesh_2, m_mesh_3 })
    {
        if (m_style_flag & FLAG_SHOW_SOLID) mesh->draw(m_obj_col, SOLID);
        if (m_style_flag & FLAG_SHOW_WIRE) mesh->draw(Colour::black, WIRE);
        if (m_style_flag & FLAG_SHOW_NORMALS) mesh->draw(Colour::black, NORMALS);
        if (m_style_flag & FLAG_SHOW_AXES) mesh->draw(Colour::black, AXES);
        if (m_style_flag & FLAG_SHOW_AABB) mesh->draw(Colour::black, BOUND);
    }

    //! Draw selected triangles
    m_canvas.draw();
}

void Simple3DScene::mousePressed(int x, int y, int modif)
{
    const bool shift_down = shiftDown(modif);

    if (!shift_down)
    {
        return Scene::mousePressed(x, y, modif);
    }
    else
    {
        selectTri(x, y);
    }

    //! Draw floor
    vector<math::Triangle> tris;
    tris.push_back(math::Triangle(B, A, D));
    tris.push_back(math::Triangle(B, D, C));
    tris.push_back(math::Triangle(F, E, A));
    tris.push_back(math::Triangle(F, A, B));

    for (const auto &t : tris)
    {
        auto floor_tri = math2vvr(t, Colour(23, 35, 56));
        floor_tri.setSolidRender(true);
        floor_tri.draw();
    }

}

void Simple3DScene::mouseMoved(int x, int y, int modif)
{
    const bool shift_down = shiftDown(modif);

    if (!shift_down)
    {
        return Scene::mouseMoved(x, y, modif);
    }
    else
    {
        selectTri(x, y);
    }
}

void Simple3DScene::mouseReleased(int x, int y, int modif)
{
    const bool shift_down = shiftDown(modif);

    if (!shift_down)
    {
        return Scene::mouseReleased(x, y, modif);
    }
    else
    {

    }
}

void Simple3DScene::keyEvent(unsigned char key, bool up, int modif)
{
    Scene::keyEvent(key, up, modif);
    key = tolower(key);

    switch (key)
    {
    case 'a': m_style_flag ^= FLAG_SHOW_AXES; break;
    case 'w': m_style_flag ^= FLAG_SHOW_WIRE; break;
    case 's': m_style_flag ^= FLAG_SHOW_SOLID; break;
    case 'n': m_style_flag ^= FLAG_SHOW_NORMALS; break;
    case 'b': m_style_flag ^= FLAG_SHOW_AABB; break;
    case 'd': m_mesh_1->getTriangles().erase(m_mesh_1->getTriangles().begin()); break;
    case 'f': m_mesh_2->getTriangles().erase(m_mesh_2->getTriangles().begin()); break;
    }
}

void Simple3DScene::selectTri(int x, int y)
{
    Ray ray = unproject(x, y);


    for (vvr::Mesh* mesh_ptr : { m_mesh_1, m_mesh_2, m_mesh_3, })
    {
        vector<vvr::Triangle3D*> tris_sel;
        int tri_min_index = -1, tri_counter = 0;
        float tri_min_dist = FLT_MAX;

        for (unsigned i = 0; i < mesh_ptr->getTriangles().size(); ++i)
        {
            vvr::Triangle &t = mesh_ptr->getTriangles()[i];

            math::Triangle tri(
                vec(t.v2().x, t.v2().y, t.v2().z),
                vec(t.v1().x, t.v1().y, t.v1().z),
                vec(t.v3().x, t.v3().y, t.v3().z)
                );

            if (tri.Intersects(ray)) {
                tris_sel.push_back(new Triangle3D(math2vvr(tri, Colour::magenta)));
                float d;
                if ((d = tri.DistanceSq(ray.pos)) < tri_min_dist) {
                    tri_min_dist = d;
                    tri_min_index = tri_counter;
                }
                tri_counter++;
            }

        }

        if (tri_min_index != -1) m_canvas.add(tris_sel.at(tri_min_index));
    }
}

int main(int argc, char* argv[])
{
    try
    {
        return vvr::mainLoop(argc, argv, new Simple3DScene);
    }
    catch (std::string exc)
    {
        cerr << exc << endl;
        return 1;
    }
}
