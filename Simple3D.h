#include <VVRScene/settings.h>
#include <VVRScene/scene.h>
#include <VVRScene/mesh.h>
#include <vector>
#include <string>
#include <MathGeoLib.h>

class Simple3DScene : public vvr::Scene 
{
public:
    Simple3DScene();
    const char* getName() const { return "Simple 3D Scene";}

protected:
    void draw() override;
    void resize() override;
    void reset() override;
    void keyEvent(unsigned char key, bool up, int modif) override;
    void mousePressed(int x, int y, int modif) override;
    void mouseMoved(int x, int y, int modif) override;
    void mouseReleased(int x, int y, int modif) override;

private:
    void selectTri(int x, int y);
    void load3DModels();
    void playWithMathGeoLibPolygon();

private:
    vvr::Mesh* m_mesh_1;
    vvr::Mesh* m_mesh_2;
    vvr::Mesh* m_mesh_3;
    vvr::Colour m_obj_col;
    vvr::Canvas2D m_canvas;
    std::vector<math::Triangle> m_floor_tris;
    int m_style_flag;
};
